import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [ 
  { 
    path: '', 
    loadChildren: './home/home.module#HomeModule', 
  },
  {
    path: 'create-blog',
    loadChildren: './create-blog/create-blog.module#CreateBlogModule'
  },
  {
    path: "edit-blog",
    loadChildren: './edit-blog/edit-blog.module#EditBlogModule'
  },
  {
    path: "edit-blog/:id",
    loadChildren: './blog-detail-edit/blog-detail-edit.module#BlogDetailEditModule'
  },
  { 
    path: 'detail/:id', 
    loadChildren: './blog-detail/blog-detail.module#BlogDetailModule'
  },
  { 
    path: '**', 
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
