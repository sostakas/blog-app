import { Component, OnInit } from '@angular/core';
import { BlogService } from '../services/blog.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  blogs = []

  constructor(
    private blogService: BlogService
  ) {}

  ngOnInit() {
    if(this.blogService.getBlogs())
      this.blogs = this.blogService.getBlogs()
  }
}