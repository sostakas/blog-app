import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'cutString'})
export class CutStringPipe implements PipeTransform {
  transform(content: string) {
    return content.substring(0,10)
  }
}