import { Component, OnInit } from '@angular/core';
import { BlogService } from '../services/blog.service';

@Component({
  selector: 'app-edit-blog',
  templateUrl: '../home/home.component.html'
})
export class EditBlogComponent implements OnInit {
  blogs = []

  constructor(
    private blogService: BlogService
  ) {}

  ngOnInit() { 
    if(this.blogService.getBlogs())
      this.blogs = this.blogService.getBlogs()
  }
  
  deleteBlog(blog, blogs) {
    blogs.splice(this.blogs.findIndex(i => i.title === blog.title), 1);
    localStorage.setItem("blogs", JSON.stringify(blogs));
    alert("Blog has been deleted");
    return blogs
  }
}