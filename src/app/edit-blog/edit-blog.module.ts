import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditBlogComponent } from './edit-blog.component';
import { Routes, RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { ApplicationPipesModule } from '../pipes/app-pipes.module';

const routes: Routes = [ 
  {
    path: '',
    component: EditBlogComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxPaginationModule,
    ApplicationPipesModule
  ],
  declarations: [ EditBlogComponent ]
})
export class EditBlogModule { }
