import { Component, OnInit } from '@angular/core';
import { BlogService } from '../services/blog.service';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { BlogUserData } from '../create-blog/blog-data.component';

@Component({
  selector: 'app-blog-detail-edit',
  templateUrl: './blog-detail-edit.component.html',
  styleUrls: ['./blog-detail-edit.component.css']
})
export class BlogDetailEditComponent implements OnInit {
  blog;
  public blogUserForm: FormGroup;
  public blogUserData: BlogUserData;

  constructor(
    fb: FormBuilder,
    private blogService: BlogService
  ) {
    this.blogUserForm = fb.group({
      title: [null],
      author: [null, [Validators.pattern
        ('.*[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð].*'),
        Validators.pattern(/.+[\s].+/)]],
      content: [null, [Validators.pattern(/.+[\s].+/)]], 
      email: [null, [Validators.pattern(/.+@.+[\.].+/)]]
    });
  }

  ngOnInit() {
    if(this.blogService.getBlogs()) 
      this.blog = this.blogService.getBlogs()[this.blogService.getRouteUrl()]      
  }

  editBlog() {
    let blogs = []
    if (this.blogUserForm.valid) {
      this.blogUserData = {
        title: this.blogUserForm.value['title'],
        author: this.blogUserForm.value['author'],
        content: this.blogUserForm.value['content'],
        email: this.blogUserForm.value['email'],
      };
      this.blogService.getBlogs().map((e, i) => {       // su splice padaryt graziai 
        if(i === +this.blogService.getRouteUrl()) {
          blogs.push(this.blogUserData);
        } else blogs.push(e)
      })
      localStorage.setItem("blogs", JSON.stringify(blogs));
      alert("Blog edited");
    } else {
      this.validateAllFormFields(this.blogUserForm);
    }
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}