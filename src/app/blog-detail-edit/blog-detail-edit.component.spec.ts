import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogDetailEditComponent } from './blog-detail-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlogService } from '../services/blog.service';
import { AppRoutingModule } from '../app-routing.module';
import { APP_BASE_HREF } from '@angular/common';

describe('BlogDetailEditComponent', () => {
  let component: BlogDetailEditComponent;
  let fixture: ComponentFixture<BlogDetailEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogDetailEditComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule
      ],
      providers: [ 
        BlogService,
        {provide: APP_BASE_HREF, useValue: '/'},
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogDetailEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
