import { NgModule } from "@angular/core";
import { CutStringPipe } from "./cut-string.pipe";

@NgModule({
  imports: [
  ],
  declarations: [ 
    CutStringPipe
  ],
  exports: [
    CutStringPipe
  ]
})
export class ApplicationPipesModule {}