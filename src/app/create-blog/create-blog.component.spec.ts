import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBlogComponent } from './create-blog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlogService } from '../services/blog.service';
import { AppRoutingModule } from '../app-routing.module';
import { APP_BASE_HREF } from '@angular/common';

describe('CreateBlogComponent', () => {
  let component: CreateBlogComponent;
  let fixture: ComponentFixture<CreateBlogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBlogComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule
      ],
      providers: [ BlogService, {provide: APP_BASE_HREF, useValue: '/'},]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
