import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogDetailEditComponent } from './blog-detail-edit.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const routes: Routes = [ 
  {
    path: '',
    component: BlogDetailEditComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ BlogDetailEditComponent ]
})
export class BlogDetailEditModule { }
