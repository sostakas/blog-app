import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { Routes, RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { CutStringPipe } from '../pipes/cut-string.pipe';
import { ApplicationPipesModule } from '../pipes/app-pipes.module';

const routes: Routes = [ 
  { 
    path: '', 
    component: HomeComponent,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxPaginationModule,
    ApplicationPipesModule
  ],
  declarations: [ 
    HomeComponent   ]
})
export class HomeModule { }
