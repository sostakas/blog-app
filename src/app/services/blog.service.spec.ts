import { TestBed, inject } from '@angular/core/testing';
import { BlogService } from './blog.service';
import { APP_BASE_HREF } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';

describe('BlogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppRoutingModule
      ],
      providers: [BlogService, {provide: APP_BASE_HREF, useValue: '/'},]
    });
  });

  it('should be created', inject([BlogService], (service: BlogService) => {
    expect(service).toBeTruthy();
  }));
});
