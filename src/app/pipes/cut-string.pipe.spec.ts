import { CutStringPipe } from "./cut-string.pipe";

describe('TitleCasePipe', () => {
    let pipe = new CutStringPipe();
   
    it('transforms "qwert12345aaa" to "qwert12345"', () => {
      expect(pipe.transform('qwert12345aaa')).toBe('qwert12345');
    });
  });