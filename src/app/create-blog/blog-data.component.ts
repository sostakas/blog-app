export class BlogUserData {
    title: string;
    author: string;
    content: string;
    email: string;
}
  