import { Component, OnInit } from '@angular/core';
import { BlogService } from '../services/blog.service';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html'
})
export class BlogDetailComponent implements OnInit {
  blog;

  constructor(
    private blogService: BlogService
  ) {}

  ngOnInit() {
    if(this.blogService.getBlogs())
      this.blog = this.blogService.getBlogs()[this.blogService.getRouteUrl()]   
  }
}
