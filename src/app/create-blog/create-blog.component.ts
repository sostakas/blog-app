import { Component, OnInit } from '@angular/core';
import { BlogService } from '../services/blog.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { BlogUserData } from './blog-data.component';

@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrls: ['./create-blog.component.css']
})
export class CreateBlogComponent implements OnInit {
  public blogUserForm: FormGroup;
  public blogUserData: BlogUserData;

  constructor(fb: FormBuilder,
    private blogService: BlogService
  ) {
    this.blogUserForm = fb.group({
      title: [null],
      author: [null, [Validators.pattern
        ('.*[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð].*'),
        Validators.pattern(/.+[\s].+/)]],
      content: [null, [Validators.pattern(/.+[\s].+/)]], 
      email: [null, [Validators.pattern(/.+@.+[\.].+/)]]
    });
   }

  ngOnInit() {
  }

  createBlog(title, content, author, email) {
    let blogs = []
    if (this.blogUserForm.valid) {
      this.blogUserData = {
        title: this.blogUserForm.value['title'],
        author: this.blogUserForm.value['author'],
        content: this.blogUserForm.value['content'],
        email: this.blogUserForm.value['email'],
      };
      if(localStorage.getItem("blogs")) {
        blogs = this.blogService.getBlogs();
        blogs.unshift(this.blogUserData) 
        localStorage.setItem("blogs", JSON.stringify(blogs));
      } else {
        blogs.push(this.blogUserData)
        localStorage.setItem("blogs", JSON.stringify(blogs));
      }
      title.value = '';
      content.value = '';
      author.value = '';
      email.value = '';
      alert("Blog submitted");      
    } else {
      this.validateAllFormFields(this.blogUserForm);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}