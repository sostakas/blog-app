import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBlogComponent } from './edit-blog.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { RouterModule } from '@angular/router';
import { ApplicationPipesModule } from '../pipes/app-pipes.module';
import { BlogService } from '../services/blog.service';
import { AppRoutingModule } from '../app-routing.module';
import { APP_BASE_HREF } from '@angular/common';

describe('EditBlogComponent', () => {
  let component: EditBlogComponent;
  let fixture: ComponentFixture<EditBlogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBlogComponent ],
      imports: [    
        NgxPaginationModule,
        RouterModule,
        ApplicationPipesModule,
        AppRoutingModule
      ],
      providers: [
        BlogService,
        {provide: APP_BASE_HREF, useValue: '/'},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should delete blog from blogs list', () => {
    let blog = {"title":"afeas","author":"sdfvdv","content":"csvsfd","email":"ernestas.sostakas@gmail.com"}
    let blogs = [
      {"title":"naujas","author":"e ars rs","content":"eafsgsrgs tdhs tgshdthfh dhtdf","email":"ernestas.sostakas@gmail.com"},
      {"title":"afeas","author":"sdfvdv","content":"csvsfd","email":"ernestas.sostakas@gmail.com"}
    ]
    expect(blogs.length).toBe(component.deleteBlog(blog, blogs).length +1);
  });
});
