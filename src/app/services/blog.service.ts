import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Injectable()
export class BlogService {

  constructor(
    private router: Router,
    private location: Location,
  ) { }

  getBlogs() {
    if(localStorage.getItem("blogs")) {
      return JSON.parse(localStorage.getItem("blogs"));
    }
  }

  getRouteUrl() {
    return this.router.url.split("/")[this.router.url.split("/").length-1];
  }

  goBack(): void {
    this.location.back();
  }

  blogIndex(blog) {
    return this.getBlogs().findIndex(i => i.title === blog.title);
  }
}